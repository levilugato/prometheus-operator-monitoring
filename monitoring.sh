#!/bin/bash

#################################################################
# Environment configuration
#################################################################
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
CYAN='\033[0;36m'


#################################################################
# Updating Helm Repository
#################################################################
tput sgr0
tput bold
echo
echo -e "${ORANGE}Updating ${GREEN}Helm Repository${ORANGE}.${CYAN}"
helm repo update


#################################################################
# Creating monitoring namespace
#################################################################
tput sgr0
echo
echo -e "${ORANGE}Creating ${GREEN}Monitoring${ORANGE} Namespace.${CYAN}"
kubectl create namespace monitoring
sleep 5

# ##################################################################
# # Helm Initialization
# ##################################################################
 tput sgr0
 tput bold
 echo "Initializing Helm and Tiller..."
 kubectl create serviceaccount tiller --namespace kube-system
 kubectl create clusterrolebinding tiller-role-binding --clusterrole cluster-admin --serviceaccount=kube-system:tiller
 helm init --service-account tiller
 sleep 10

#################################################################
# Installing Prometheus Operator Helm Chart
#################################################################
tput sgr0
tput bold
echo
echo -e "${ORANGE}Installing ${GREEN}Prometheus Operator ${ORANGE}Helm Chart.${CYAN}"
helm upgrade --install prometheus-operator stable/prometheus-operator  \
    --namespace monitoring \
    --values custom-values.yaml \
    --force \
    --recreate-pods 
   

#################################################################
# Installing Metrics-Server Helm Chart

tput sgr0
tput bold
echo
echo -e "${ORANGE}Installing ${GREEN}Metrics-Server ${ORANGE}Helm Chart.${CYAN}"
helm upgrade --install metrics-server stable/metrics-server \
    --namespace kube-system \
    --values helm-charts/metrics-server/values.yaml \
    --force \
    --recreate-pods \
    --version "2.8.0"


#################################################################
# Install custom-kube-prometheus generated alerts

tput sgr0
tput bold
# File generated from: https://bitbucket.org/ifood/custom-kube-prometheus
echo
echo -e "${ORANGE}Installing ${GREEN}Custom Kube Prometheus ${ORANGE}Generated alerts.${CYAN}"
kubectl apply \
    --namespace monitoring \
    --filename ./monitoring/manifests/custom-prometheus-rules.yaml


sleep 15
rm -rf ./custom-values.tmp 
#################################################################
# Finishing
#################################################################
tput sgr0
echo
echo -e "${GREEN}... done!"