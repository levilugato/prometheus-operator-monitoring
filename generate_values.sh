#!/bin/bash

set -e

if [ "$1" == "" ]
then
	echo -e "Please, set your cloud choice : (do, aws)"
	exit
fi

read -p "EMAIL TO:  " EMAIL_TO
read -p "EMAIL FROM:  " EMAIL_FROM
read -p "EMAIL PASS APP:  " EMAIL_PASS_APP
read -p "SMTP SERVER:  " SMTP_SERVER
read -p "GRAFANA PASS:  " GRAFANA_PASS
read -p "SLACK HOOK:  " SLACK_HOOK
read -p "SLACK CHANNEL:  " SLACK_CHANNEL

if [ "$1" == "do" ] 
then
    echo "$(cat monitoring/manifests/custom-values-do.tpl.yaml)" > custom-values.tmp
    echo "$(cat monitoring/manifests/custom-values-default.tpl.yaml)" >> custom-values.tmp
fi

if [ "$1" == "aws" ] 
then
    echo "$(cat monitoring/manifests/custom-values-aws.tpl.yaml)" > custom-values.tmp
    echo "$(cat monitoring/manifests/custom-values-default.tpl.yaml)" >> custom-values.tmp
fi


sed 's|_EMAIL_TO_|'"${EMAIL_TO}"'|g; s|_EMAIL_FROM_|'"${EMAIL_FROM}"'|g; s|_EMAIL_PASS_APP_|'"${EMAIL_PASS_APP}"'|g; s|_GRAFANA_PASS_|'"${GRAFANA_PASS}"'|g; s|_SLACK_HOOK_|'"${SLACK_HOOK}"'|g; s|_SLACK_CHANNEL_|'"${SLACK_CHANNEL}"'|g; s|_SMTP_SERVER_|'"${SMTP_SERVER}"'|g;' custom-values.tmp > custom-values.yaml;

